import {createStore} from 'redux'
import React from 'react'
import ReactDOM from 'react-dom'

// Counterコンポーネント(extends Reactしてないけど、これもコンポーネントでいいのかな？)
// なんか記法がわからない
// 引数を要素として出力したい場合は、配列として渡す必要がある(={}で囲む)
const Counter = ({
                     value,
                     onIncrement,
                     onDecrement
                 }) => (
    <div>
        <h1>{value}</h1>
        <button onClick={onIncrement}>+</button>
        <button onClick={onDecrement}>-</button>
    </div>
);


// Reducerの実装
const counter = (state = 0, action) => {
    switch (action.type) {
        case 'INCREMENT':
            return state + 1;
        case 'DECREMENT':
            return state - 1;
        default:
            return state;
    }
};

// counter関数(reducer)のstore
const store = createStore(counter);

// reactでonClickイベントを登録してるのでコメントアウト
//documentオブジェクトをクリックしたらINCREMENTアクションをdispatchする
//イベントを追加
// document.addEventListener('click', () => {
//     store.dispatch({type: 'INCREMENT'})
// });


const onIncrement = () => {
    store.dispatch({
        type: 'INCREMENT'
    })
};

const onDecrement = () => {
    store.dispatch({
        type: 'DECREMENT'
    })
};


//画面更新用の関数を作成
const render = () => {
    ReactDOM.render(
        <Counter
            value={store.getState()}
            onIncrement={onIncrement}
            onDecrement={onDecrement}
        />,
        document.getElementById('root')
    )
}

//subscribe関数に、現在のstateの状況を画面に表示する関数をセット
store.subscribe(render);


render();