import {createStore} from 'redux'

// Reducerの実装
const counter = (state = 0, action) => {
    switch (action.type) {
        case 'INCREMENT':
            return state + 1;
        case 'DECREMENT':
            return state - 1;
        default:
            return state;
    }
};

// 画面更新用関数
const render = () => {
    document.body.innerText = store.getState();
};

// counter関数(reducer)のstore
const store = createStore(counter);

//subscribe関数に、現在のstateの状況を画面に表示する関数をセット
store.subscribe(render);

//documentオブジェクトをクリックしたらINCREMENTアクションをdispatchする
//イベントを追加
document.addEventListener('click', () => {
    store.dispatch({type: 'INCREMENT'})
});

// 最初の画面表示(0が表示される)
render();


// // counterの現在のstate
// console.log(store.getState());

// dipatch(アクション)で、reducerに現在のstateとactionを送る
// store.dispatch({type: 'INCREMENT'});

// // counterの現在のstate
// console.log(store.getState());

// reducerのテストコード
import expect from 'expect'

// // 0とINCREMENTを渡すと、1加算されて1が返却される
// expect(
//     counter(0, {type: 'INCREMENT'})
// ).toEqual(1);
//
// // 1とINCREMENTを渡すと、1加算されて2が返却される
// expect(
//     counter(1, {type: 'INCREMENT'})
// ).toEqual(2);
//
// // 2とDECREMENTを渡すと、1減算されて1が返却される
// expect(
//     counter(2, {type: 'DECREMENT'})
// ).toEqual(1);
//
// // 1と未定義のアクションを渡すと、1が返却される
// expect(
//     counter(1, {type: 'UNKNOWN'})
// ).toEqual(1);
//
// // undefinedとアクションが未設定の場合は、0が返却される
// expect(
//     counter(undefined, {})
// ).toEqual(0);
//
// console.log('test passed!!');