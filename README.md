## 概要

reduxの勉強する

これ:  
https://redux.js.org/basics/actions  
↑よりも↓のほうがよさそう  
TDDで説明しててすごくいい!!!  
https://qiita.com/insight3110/items/4d212ecef6992e8eaee5

一通りできたら、他の参考サイトで勉強
* https://egghead.io/lessons/react-redux-writing-a-todo-list-reducer-adding-a-todo


## 基礎

スプレッド構文
---
配列の要素を引数として渡せる
```
const func1 = (one, two) => {}
const list = [{id:0}, {id:1}]

func1(...list)
```
配列のコピー
```
const list = [{id:0}, {id:1}]
const newList = [...list]
```

スプレッド構文で展開して、値を上書き
```
const state = [
    {
        id: 0,
        text: 'Learn Redux',
        completed: false
    },
    {
        id: 1,
        text: 'Go shopping',
        completed: false
    }
];

const newState = state.map(
    todo => {
        if (todo.id !== 1) {
            return todo;
        }

        console.log({...todo});
        return {
            ...todo,
            completed: !todo.completed
        };
    }
);
```


### 環境準備


もろもろインストール
```
npm init -y
npm i -D babel-cli babel-loader babel-preset-env babel-preset-react expect react react-dom react-redux redux webpack webpack-cli

```

babel用設定ファイル配置
```
touch .bablerc
```
```
(.babelrc)

{
  "presets": ["env", "react"]
}
```

webpack用設定ファイル配置
```
touch webpack.config.js
```
```
(webpack.config.js)

module.exports={
  mode: "development",
  entry: ["./src/app.js"],
  output: {
    path: __dirname+"/dist",
    filename: "app.js"
  },
  module: {
    rules: [
      {
        resource: {
          test: /\.jsx?$/,
          exclude: /node_modules/
        },
        use: ["babel-loader"]
      }
    ]
  }
}
```

設定にあわせてフォルダ作成
```
mkdir src
mkdir dist
```

追記:  
以下のようなエラーがでてうまくいかなかった
```
ERROR in ./src/app.js
Module build failed (from ./node_modules/babel-loader/lib/index.js):
Error: Plugin/Preset files are not allowed to export objects, only functions.
```

最終的に、この記事参考にパッケージのバージョンとか修正  
https://stackoverflow.com/questions/49182862/preset-files-are-not-allowed-to-export-objects  
うまくいった
```
(package.json)

{
  "name": "redux-tutorial",
  "version": "1.0.0",
  "description": "reduxの勉強する",
  "main": "webpack.config.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "repository": {
    "type": "git",
    "url": "git+https://mihirogi@bitbucket.org/mihirogi/redux-tutorial.git"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "homepage": "https://bitbucket.org/mihirogi/redux-tutorial#readme",
  "devDependencies": {
    "@babel/core": "^7.0.0-beta.40",
    "@babel/cli": "^7.0.0-beta.40",
    "babel-loader": "^8.0.2",
    "babel-preset-env": "^1.7.0",
    "@babel/preset-react": "^7.0.0-beta.40",
    "expect": "^23.6.0",
    "react": "^16.5.0",
    "react-dom": "^16.5.0",
    "react-redux": "^5.0.7",
    "redux": "^4.0.0",
    "webpack": "^4.18.0",
    "webpack-cli": "^3.1.0"
  }
}
```
```
(.babelrc)

{
  "presets": ["@babel/react"]
}

```

### 実行

ビルド
```
npx webpack -w
```

(別プロンプトで)
```
node dist/app.js
```

### エラー

`...list`を使うとエラーがでる
---
```
Error: Module build failed (from ./node_modules/babel-loader/lib/index.js):
SyntaxError: /Users/UserName/work/redux/redux-tutorial/src/todolist.js: Support for the experimental syntax 'objectRestSpread' isn't currently enabled (36:21):

  34 |
  35 |                 return {
> 36 |                     ...todo,
     |                     ^
  37 |                     completed: !todo.completed
  38 |                 };
  39 |             });

Add @babel/plugin-proposal-object-rest-spread (https://git.io/vb4Ss) to the 'plugins' section of your Babel config to enable transformation.
```
このあたり参考にした:  
https://babeljs.io/docs/en/next/babel-plugin-proposal-object-rest-spread  
https://github.com/babel/babel/issues/6970

以下実行
`npm install --save-dev @babel/plugin-proposal-object-rest-spread`
```
(.babelrc)

{
  "presets": [
    "@babel/react"
  ],
  "plugins": [
    "@babel/plugin-proposal-object-rest-spread"
  ]
}

```

### reducer

Reduxの３原則

1. 状態(State)を保管する場所であるStoreは１つだけ
2. 状態(State)は基本読み取りのみ。書き込むにはStoreが提供するメソッドであるDispatchでActionを投げる時だけ
3. 実際にActionを受け取ってStateを変更するのはReducerが受け持つが、Reducerは副作用を持たない関数でなければならない


用語
* `State`  
* `Store`
* `Action`
* `Reducer`
* `dispatch`
* `subscribe`

State
---
reactでいう`this.state`のような状況によって、変遷するパラメータ

Store
---
`State`を管理する場所。１つだけ存在する(singleton?)

Action
---
なんの動作か説明するオブジェクト  
`type`というプロパティは必須
```
(例)

{
 type:'ADD_TODO',
 text:'追加するtodoのテキスト'
}
```

Reducer
---
現在の`State`と`Action`を受け取って、Actionに応じて変化した`State`を返却する関数  
副作用を持たない純関数である必要があるため、引数の`State`を直接いじらず、状態を変化させた新しい`State`を返却する

※副作用...  
https://qiita.com/NomuraS/items/d5e08d83ba1825d33c87
↑をよんでみた  
ひとまず、以下3要素が守られていたら純粋関数とする。  

* (外部からの入力)グローバル変数やdocument.getElementByIdなどの関数の引数以外の入力に出力が依存している
* (外部への出力)console.logなどの関数外部への出力
* (外部の操作)グローバル変数に対する代入などの操作

dispatch
---
`Action`を引数として受けて、`Reducer`に現在の`State`と`Action`を渡す

subscribe
---
関数を引数にとり、`dispach`が実行される都度、引数の関数を実行する。

### deep freeze

不変オブジェクトを作るようらしい
JavaScript組み込みなら、以下で作れるらしい

```
Object.freeze()
```

deep-freezeっていうライブラリもある

