module.exports={
    mode: "development",
    entry: ["./src/todolist.js"],
    output: {
        path: __dirname+"/dist",
        filename: "todolist.js"
    },
    module: {
        rules: [
            {
                resource: {
                    test: /\.jsx?$/,
                    exclude: /node_modules/
                },
                use: ["babel-loader"]
            }
        ]
    }
}